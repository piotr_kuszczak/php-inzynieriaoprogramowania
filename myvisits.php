<?php require('includes/config.php');

if (! $user->is_logged_in()){
    header('Location: login.php'); 
    exit(); 
}

$title = 'Moje wizyty';

require('layout/header.php'); 

  $id = $_SESSION['id'];
  $now = date("Y-m-d H:i:s");
  
  echo "Odbyte wizyty: </br>" ;
  
  $stmt = $db->prepare("SELECT * FROM visits WHERE user_id = :id AND date < :date");
  $stmt->bindValue(':id', $id, PDO::PARAM_STR);
  $stmt->bindValue(':date', $now, PDO::PARAM_STR);
  $result = $stmt->execute();

  if($result !== false)
  {
    while($row = $stmt->fetch())
        {
              echo 'Lekarz: ' . $row['doctor_id']  .
              ', Data:' . $row['date']  .
              "<a href=addopinion.php?ID=".$row["id"]."&DoctorID=".$row["doctor_id"].">Dodaj opinie</a>".
              '<br />';
        }
  }
        
  $stmt->closeCursor();
  unset($stmt);
  
  echo "Nadchodzące wizyty: </br>" ;
  
  $stmt = $db->prepare("SELECT * FROM visits WHERE user_id = :id AND date >= :date");
  $stmt->bindValue(':id', $id, PDO::PARAM_STR);
  $stmt->bindValue(':date', $now, PDO::PARAM_STR);
  $result = $stmt->execute();
  
  if($result !== false)
  {
    while($row = $stmt->fetch())
        {
              echo 'Lekarz: ' . $row['doctor_id']  .
              ', Data:' . $row['date']  .
              '<br />';
        }
  }

  $stmt->closeCursor();
  unset($stmt);
  $db = null; 
?>

<?php 
//include header template
require('layout/header.php');
?>