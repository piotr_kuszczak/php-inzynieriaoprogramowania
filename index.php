<?php require('includes/config.php');

if ($user->is_logged_in() ){ 
	header('Location: memberpage.php'); 
	exit(); 
}

if(isset($_POST['submit'])){

    if (! isset($_POST['login'])) {
    	$error[] = "Błędny login";
    }

    if (! isset($_POST['email'])) {
    	$error[] = "Błędny email";
    }

    if (! isset($_POST['password'])) {
    	$error[] = "Błędne hasło";
    }

    if (! isset($_POST['uname'])) {
        $error[] = "Wpisz Imię";
    }

    if (! isset($_POST['surname'])) {
        $error[] = "Wpisz nazwisko";
    }

    if (! isset($_POST['pesel'])) {
        $error[] = "Wpisz PESEL";
    }

    $login = $_POST['login'];
    $uname = $_POST['uname'];
    $surname = $_POST['surname'];
    $pesel = $_POST['pesel'];

	if (! $user->isValidUser(login)){
		$error[] = 'Login musi zawierać min. 3 znaki';
	} else {
		$stmt = $db->prepare('SELECT login FROM users WHERE login = :login');
		$stmt->execute(array(':login' => login));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if (! empty($row['login'])){
			$error[] = 'Login jest już używany.';
		}
	}

	if (strlen($_POST['password']) < 3){
		$error[] = 'Hasło jest za krótkie';
	}

	if (strlen($_POST['passwordConfirm']) < 3){
		$error[] = 'Hasło jest za krótkie';
	}

	if ($_POST['password'] != $_POST['passwordConfirm']){
		$error[] = 'Hasła nie są zgodne';
	}

	$email = htmlspecialchars_decode($_POST['email'], ENT_QUOTES);
	if (! filter_var($email, FILTER_VALIDATE_EMAIL)){
	    $error[] = 'Wprowadzono niepoprawny email';
	} else {
		$stmt = $db->prepare('SELECT email FROM users WHERE email = :email');
		$stmt->execute(array(':email' => $email));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if (! empty($row['email'])){
			$error[] = 'Email jest już używany';
		}
	}

	if (!isset($error)){

		$hashedpassword = password_hash($_POST['password'], PASSWORD_BCRYPT);

		$activasion = md5(uniqid(rand(),true));

		try {

			$stmt = $db->prepare('INSERT INTO users (login,uname,surname,password,email,pesel,active) VALUES (:login, :uname, :surname, :password, :email, :pesel, :active)');
			$stmt->execute(array(
				':login' => $login,
                ':uname' => $uname,
                ':surname' => $surname,
                ':password' => $hashedpassword,
                ':email' => $email,
                ':pesel' => $pesel,
				':active' => $activasion
			));
			$id = $db->lastInsertId('id');

			$to = $_POST['email'];
			$subject = "Registration Confirmation";
			$body = "<p>Na maila został wysłany link aktywacyjny <a href='".DIR."activate.php?x=$id&y=$activasion'>".DIR."activate.php?x=$id&y=$activasion</a></p>";

			$mail = new Mail();
			$mail->setFrom(SITEEMAIL);
			$mail->addAddress($to);
			$mail->subject($subject);
			$mail->body($body);
			$mail->send();

			header('Location: index.php?action=joined');
			exit;

		} catch(PDOException $e) {
		    $error[] = $e->getMessage();
		}
	}
}

$title = 'Rejestracja';

require('layout/header.php');
?>


<div class="container">

	<div class="row">

	    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			<form role="form" method="post" action="" autocomplete="off">
				<h2>Rejestracja</h2>
				<p><a href='login.php'>Zaloguj się</a></p>
				<hr>

				<?php
				if(isset($error)){
					foreach($error as $error){
						echo '<p class="bg-danger">'.$error.'</p>';
					}
				}

				if(isset($_GET['action']) && $_GET['action'] == 'joined'){
					echo "<h2 class='bg-success'>Rejestracja przebiegła pomyślnie</h2>";
				}
				?>

				<div class="form-group">
					<input type="text" name="login" id="login" class="form-control input-lg" placeholder="Nazwa użytkownika" value="<?php if(isset($error)){ echo htmlspecialchars($_POST['login'], ENT_QUOTES); } ?>" tabindex="1">
				</div>
                <div class="form-group">
                    <input type="text" name="uname" id="uname" class="form-control input-lg" placeholder="Imie" value="<?php if(isset($error)){ echo htmlspecialchars($_POST['uname'], ENT_QUOTES); } ?>" tabindex="2">
                </div>
                <div class="form-group">
                    <input type="text" name="surname" id="surname" class="form-control input-lg" placeholder="Nazwisko" value="<?php if(isset($error)){ echo htmlspecialchars($_POST['surname'], ENT_QUOTES); } ?>" tabindex="3">
                </div>
                <div class="form-group">
                    <input type="text" name="pesel" id="pesel" class="form-control input-lg" placeholder="PESEL" value="<?php if(isset($error)){ echo htmlspecialchars($_POST['pesel'], ENT_QUOTES); } ?>" tabindex="4">
                </div>
                <div class="form-group">
                    <input type="email" name="email" id="email" class="form-control input-lg" placeholder="Adres Email" value="<?php if(isset($error)){ echo htmlspecialchars($_POST['email'], ENT_QUOTES); } ?>" tabindex="5">
                </div>
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6">
						<div class="form-group">
							<input type="password" name="password" id="password" class="form-control input-lg" placeholder="Hasło" tabindex="7">
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6">
						<div class="form-group">
							<input type="password" name="passwordConfirm" id="passwordConfirm" class="form-control input-lg" placeholder="Powtórz Hasło" tabindex="8">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-6 col-md-6"><input type="submit" name="submit" value="Zarejestruj" class="btn btn-primary btn-block btn-lg" tabindex="9"></div>
				</div>
			</form>
		</div>
	</div>

</div>

<?php
require('layout/footer.php');
?>
