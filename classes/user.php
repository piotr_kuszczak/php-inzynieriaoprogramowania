<?php

class User 
{
    public $_db;

    function __construct($db)
    {
    	$this->_db = $db;
    }

	private function get_user_hash($login)
	{
		try {
			$stmt = $this->_db->prepare('SELECT password, login, id FROM users WHERE login = :login AND active="Yes" ');
			$stmt->execute(array('login' => $login));
			return $stmt->fetch();

		} catch(PDOException $e) {
		    echo '<p class="bg-danger">'.$e->getMessage().'</p>';
		}
	}

	public function isValidUser($login)
	{
		if (strlen($login) < 3) {
			return false;
		}

		if (strlen($login) > 17) {
			return false;
		}

		if (! ctype_alnum($login)) {
			return false;
		}

		return true;
	}

	public function login($login, $password)
	{
		if (! $this->isValidUser($login)) {
			return false;
		}

		if (strlen($password) < 3) {
			return false;
		}

		$row = $this->get_user_hash($login);

		if (password_verify($password, $row['password']) == 1){

		    $_SESSION['loggedin'] = true;
		    $_SESSION['login'] = $row['login'];
		    $_SESSION['id'] = $row['id'];
		    $name = "user_id";
                    $value = trim($_GET['ID']);
                    setcookie($name, $value, time()+30);

		    return true;
		}
	}

	public function logout()
	{
		session_destroy();
	}

	public function is_logged_in()
	{
		if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true){
			return true;
		}
	}

}
