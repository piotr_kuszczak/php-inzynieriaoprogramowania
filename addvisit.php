<?php

require('includes/config.php');

if (! $user->is_logged_in()){
    header('Location: login.php'); 
    exit(); 
}

$title = 'Dodanie wizyty';

require('layout/header.php'); 

    $doctor_id = $_COOKIE["DoctorID"];
    $user_id = $_SESSION['id'];
    $date = $_POST['date'];
    

            $stmt1 = $db-> prepare("SELECT * FROM visits WHERE doctor_id = :doctor_id AND date=:date"); 
            $stmt1->bindValue(':doctor_id', $doctor_id, PDO::PARAM_STR);
            $stmt1->bindValue(':date', $date, PDO::PARAM_STR);
            $stmt1->execute(); 
            $number_of_rows = $stmt1->rowCount();


            if($number_of_rows == 0){
                
                $stmt2 = $db->prepare("INSERT INTO `visits`(`user_id`, `doctor_id`, `date`) VALUES (:user_id, :doctor_id, :date)");
                $stmt2->bindValue(':user_id', $user_id, PDO::PARAM_STR);
                $stmt2->bindValue(':doctor_id', $doctor_id, PDO::PARAM_STR);
                $stmt2->bindValue(':date', $date, PDO::PARAM_STR);
                $stmt2->execute();
                $result2 =  $stmt2->fetch(PDO::FETCH_ASSOC);
                
                $stmt2->closeCursor();
                unset($stmt2);
                
                echo "<h1>Dodano wizytę</h1></br>".
                        "<a href='myvisits.php' class='btn btn-primary'>Moje wizyty</a>";
            }
            else
            {
                echo "<h1>Termin jest już zajęty</h1></br>";
                echo "<input type='button' value='Wróć' class='btn btn-primary' onclick='history.back()'>";
            }
          
        $stmt1->closeCursor();
        unset($stmt1);

        $db = null; 

?>