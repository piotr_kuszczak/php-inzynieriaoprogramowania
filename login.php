<?php
require_once('includes/config.php');

if( $user->is_logged_in() ){ header('Location: index.php'); exit(); }

if(isset($_POST['submit'])){

	if (! isset($_POST['login'])) {
		$error[] = "Nie podano loginu lub hasła";
	}

	if (! isset($_POST['password'])) {
		$error[] = "Nie podano loginu lub hasła";
	}

	$login = $_POST['login'];
	if ($user->isValidUser($login)){
		if (! isset($_POST['password'])){
			$error[] = 'A password must be entered';
		}

		$password = $_POST['password'];

		if ($user->login($login, $password)){
			$_SESSION['login'] = $login;
			header('Location: memberpage.php');
			exit;

		} else {
			$error[] = 'Błędny login lub hasło.';
		}
	}else{
		$error[] = 'Błędny login lub hasło';
	}

}

$title = 'Login';

require('layout/header.php'); 
?>

	
<div class="container">

	<div class="row">

	    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			<form role="form" method="post" action="" autocomplete="off">
				<h2>Zaloguj się</h2>
				<p><a href='./'>Powrót do strony głównej</a></p>
				<hr>

				<?php
				//check for any errors
				if (isset($error)){
					foreach ($error as $error){
						echo '<p class="bg-danger">'.$error.'</p>';
					}
				}

				if (isset($_GET['action'])){

					//check the action
					switch ($_GET['action']) {
						case 'active':
							echo "<h2 class='bg-successKonto jest już aktywne.</h2>";
							break;
						case 'reset':
							echo "<h2 class='bg-success'>Proszę sprawdzić przesłany link.</h2>";
							break;
						case 'resetAccount':
							echo "<h2 class='bg-success'>Hasło zostało zmienione.</h2>";
							break;
					}

				}

				
				?>

				<div class="form-group">
					<input type="text" name="login" id="login" class="form-control input-lg" placeholder="Login" value="<?php if(isset($error)){ echo htmlspecialchars($_POST['login'], ENT_QUOTES); } ?>" tabindex="1">
				</div>

				<div class="form-group">
					<input type="password" name="password" id="password" class="form-control input-lg" placeholder="Hasło" tabindex="3">
				</div>
				
				<div class="row">
					<div class="col-xs-9 col-sm-9 col-md-9">
						 <a href='reset.php'>Przypomnij hasło</a>
					</div>
				</div>
				
				<hr>
				<div class="row">
					<div class="col-xs-6 col-md-6"><input type="submit" name="submit" value="Zaloguj" class="btn btn-primary btn-block btn-lg" tabindex="5"></div>
				</div>
			</form>
		</div>
	</div>



</div>


<?php 
//include header template
require('layout/footer.php'); 
?>
