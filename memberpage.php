<?php require('includes/config.php');

if (! $user->is_logged_in()){
    header('Location: login.php'); 
    exit(); 
}

if($_SERVER['REQUEST_METHOD']=="POST") {
    try {
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $city = $_POST['city'];
        $dsurname = $_POST['dsurname'];
        $specialization = $_POST['specialization'];
        
        if (!isset($_POST['city'])) {
            $error[] = "Podaj Miasto";
        }

        if (!isset($_POST['specialization'])) {
            $error[] = "Podaj specjalizację";
        }
    
       
        $stmt = $db->prepare("SELECT dname, dsurname, specialization, id FROM doctors WHERE city = :city AND specialization = :specialization");
        $stmt->bindValue(':city', $city, PDO::PARAM_STR);
        $stmt->bindValue(':specialization', $specialization, PDO::PARAM_STR);
        $result = $stmt->execute();
        
        if($result !== false)
        {
            while($row = $stmt->fetch())
            {
              echo 'Imie: ' . $row['dname']  .
              ', Nazwisko:' . $row['dsurname']  .
              ', Specjalizacja:' . $row['specialization'] .
              "<a href=doctorpage.php?ID=".$row["id"].">Więcej</a>".
              '<br />';
            }
        }
        
        $stmt->closeCursor();
        unset($stmt);
        $db = null; 
    } 
    catch (PDOException $e) {
        $error[] = $e->getMessage(); 
    }
}

$title = 'Wyszukaj lekarza';

require('layout/header.php'); 
?>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <form role="form" method="post" action="" autocomplete="off">
                <div class="container">
                    <div class="row">
                        <div class="form-group">
                            <input type="text" name="city" id="city" class="form-control input-lg" placeholder="Miasto" value="<?php { if(isset($error)) echo htmlspecialchars($_POST['city'], ENT_QUOTES); } ?>" tabindex="1">
                        </div>
                   <div class="form-group">
                             <input type="text" name="specialization" id="specialization" class="form-control input-lg" placeholder="Specjalizacja" value="<?php {  if(isset($error))echo htmlspecialchars($_POST['specialization'], ENT_QUOTES); } ?>" tabindex="2">
                   </div>
                   <div class="row">
                   <div class="col-xs-6 col-md-6">
                        <input type="submit" name="submit" value="Szukaj" class="btn btn-primary btn-block btn-lg" tabindex="9"></div>
                   </div>
                   </div>
            </form>
            <div class="container" name="result"></div>
                </div>
                <a href="myvisits.php" class="btn btn-primary">Moje wizyty</a>

        </div>
    </div>
</div>
<?php 
//include header template
require('layout/header.php');
?>