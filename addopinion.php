<?php

require('includes/config.php');

if (! $user->is_logged_in()){
    header('Location: login.php'); 
    exit(); 
}
$title = 'Dodanie opinii';

require('layout/header.php'); 

$queries = array();
parse_str($_SERVER['QUERY_STRING'], $queries);
$x = $queries['ID'];
$doctor_id = $queries['DoctorID'];
$opinion = trim($_POST['opinion']);

echo $opinion;
    if($_SERVER['REQUEST_METHOD']=="POST") {
        try {
          $user_id = $_SESSION['id'];
          $opinion = $_POST['opinion'];
          $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
          $stmt = $db->prepare("INSERT INTO `opinions`(`author_id`, `doctor_id`, `opinion`) VALUES (:user_id, :doctor_id, :opinion)");
          $stmt->bindValue(':user_id', $user_id, PDO::PARAM_STR);
          $stmt->bindValue(':doctor_id', $doctor_id, PDO::PARAM_STR);
          $stmt->bindValue(':opinion', $opinion, PDO::PARAM_STR);
          $stmt->execute();
          $result = $stmt->fetch(PDO::FETCH_ASSOC);
          echo "<h1>Dodano opinie</h1>";
        } catch (Exception $ex) {

        }
    }  
?>

<html>
            <form role="form" method="post" action="" autocomplete="off">
                <div class="container">
                    <div class="row">
                        <div class="form-group">
                            <input type="text" name="opinion" id="opinion" class="form-control input-lg" placeholder="Dodaj opinie" 
                                   value="<?php { if(isset($error)) echo htmlspecialchars($_POST['opinion'], ENT_QUOTES); } ?>" tabindex="1">
                        </div>
                   <div class="col-xs-6 col-md-6">
                        <input type="submit" name="submit" value="Dodaj" class="btn btn-primary btn-block btn-lg" tabindex="9"></div>
                   </div>
                   </div>
            </form>
</html>